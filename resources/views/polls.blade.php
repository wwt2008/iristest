@extends('layouts.main')

@section('content')
    <h1 class="page-header">Polls</h1>
    <div class="row">
        <div class="col-md-6">
            @if(count($objects)>0)
            <ol>
                @foreach($objects as $object)
                    <li>
                        {{$object->content}}
                        <a class="object_link"
                           data-id="{{$object->id}}"
                           data-type="poll" href="#">
                            Load parameters
                        </a>
                        <a target="_blank" href="https://polldaddy.com/polls/{{$object->id}}/edit/">
                            Edit
                        </a>

                    </li>
                @endforeach
            </ol>
                @else
            <p>You haven't got any polls, yet.</p>
                @endif
        </div>
        <div id="object_div" class="col-md-6">

        </div>
    </div>



@endsection