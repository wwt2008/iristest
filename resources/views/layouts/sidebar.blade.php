
<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">

            <li>
                <a href="/"><i class="fa fa-home fa-fw"></i> Home</a>
            </li>
            <li>
                <a href="/surveys"><i class="fa fa-edit fa-fw"></i> Surveys</a>
            </li>
            <li>
                <a href="/polls"><i class="fa fa-list fa-fw"></i> Polls</a>
            </li>
            <li>
                <a href="/quizzes"><i class="fa fa-info fa-fw"></i> Quizzes</a>
            </li>
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>