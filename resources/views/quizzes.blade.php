@extends('layouts.main')

@section('content')
    <h1 class="page-header">Quizzes</h1>
    <div class="row">
        <div class="col-md-6">
            <ol>
                @foreach($objects as $object)
                    <li>
                        {{$object->title}}
                        <a target="_blank" href="https://polldaddy.com/quizzes/{{$object->id}}/edit/">
                            Edit
                        </a>
                    </li>
                @endforeach
            </ol>
        </div>
        <div id="object_div" class="col-md-6">

        </div>
    </div>
@endsection