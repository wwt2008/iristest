@extends('layouts.main')

@section('content')
    <h1 class="page-header">Hello!</h1>
<p>This is a simple web application which must show my skills in: </p>
    <ul>
        <li>PHP programming using OOP and PDO.</li>
        <li>Working with APIs.</li>
        <li>Javascript, jQuery and AJAX.</li>
        <li>Git and Bitbucket.</li>
        <li>Laravel 5.6</li>
    </ul>
    <p>Sorry for little delay. I just wanted my test apllication be really needed for Iris CRM. That's why
        I began from analysis of the Iris CRM and it's competitors and found that Iris CRM hasn't integrations with
    any service for working with surveys and polls (I think, they may be pretty powerfull marketing tool).
        After that I started a research and the most interesting (and free) service was
        <a href="https://polldaddy.com" target="_blank">Polldaddy.com</a>. Currently it's a simple application which can
        get lists of the surveys, polls and quizzes (which I've created before) from Polldaddy.com using its API.
        Also, it can get a list of polls parameter (using jQuery and AJAX).
    </p>
    <p>The application was developed using the Laravel 5.6 framework, that's why I used Eloquent ORM (instead of PDO) to work with
        database, but I've duplicated some requests using PDO in the comments.
    </p>
    <p>To use the application you have to click a "Log in" command in the user menu <i class="fa fa-user fa-fw"></i> and enter
    e-mail - admin@trachov.xyz and password - admin2018</p>
    <p>Full code of the application you can get here: <a target="_blank" href="https://bitbucket.org/wwt2008/iristest">Bitbucket repository</a>.</p>
    <p>And here are the most interesting (in my opinion) scripts:
        <a target="_blank" href="https://bitbucket.org/wwt2008/iristest/src/master/resources/views/layouts/main.blade.php">Main template of pages</a>,
        <a target="_blank" href="https://bitbucket.org/wwt2008/iristest/src/master/app/Http/Controllers/SurveyController.php">Controller (with main PHP code)</a></p>
@endsection