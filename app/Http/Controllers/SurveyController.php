<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Closure;
use Illuminate\Support\Facades\Session;

class SurveyController extends Controller
{

    public $userCode;
    protected $pd_email;
    protected $pd_apikey;
    protected $pd_password;
    protected $errors = [];

    public function __construct(Request $request){



        /*
        * Check if user is authorized, if no - he will be redirected to the Home page with a message
        */

        $this->middleware(function($request,Closure $next) {
            if (\Auth::check()){

/*               It's just an example how to get user object using PDO

                $DB_CONNECTION = env('DB_CONNECTION', false);
                $DB_HOST = env('DB_HOST', false);
                $DB_DATABASE = env('DB_DATABASE', false);
                $DB_USERNAME = env('DB_USERNAME', false);
                $DB_PASSWORD = env('DB_PASSWORD', false);

                $pdo = new \PDO("$DB_CONNECTION:host=$DB_HOST;dbname=$DB_DATABASE", $DB_USERNAME, $DB_PASSWORD);
                $stmt = $pdo->prepare('SELECT * FROM users WHERE id = :id LIMIT 1');
                $id = \Auth::user()->id;
                $stmt->bindParam(':id', $id, \PDO::PARAM_INT); 
                $stmt->execute();
                $user = $stmt->fetch(\PDO::FETCH_OBJ);
*/


                    $user=\Auth::user();
                    $this->pd_email = $user->pd_email;
                    $this->pd_apikey = $user->pd_apikey;
                    $this->pd_password = $user->pd_password;
                    if(false == isset($this->userCode)){
                        $userCode = $this->getUsercode();
                        if('error' != $userCode) $this->userCode = $userCode;
                        else $this->errors[]='no_usercode';
                    }

                return $next($request);
            }
            else return redirect('/')->with(['warning'=>'Sorry, but to use the application You have to Log in.']);
        },  ['except' => 'index']);

    }

    function index(){
        return view('index');
    }
    function surveys(){
        $objects = $this->getObjects('Surveys');
        return view('surveys')->with(compact('objects'));
    }
    function polls(){
        $objects = $this->getObjects('Polls');

        return view('polls')->with(compact('objects'));
    }
    function quizzes(){
        $objects = $this->getObjects('Quizzes');
        //dd($objects);
        return view('quizzes')->with(compact('objects'));
    }
    function apikey(){
        return view('apikey');
    }
    /*
     * Get information about an object.
     */
    function getObject(Request $request){
        $type = $request->get('type');
        $id = $request->get('id');
        $curl_data = json_encode(
            array(
                "pdRequest" => array(
                    "partnerGUID" => $this->pd_apikey,
                    "userCode" => $this->userCode,
                    "demands" => array(
                        "demand" => array(
                            $type => array(
                                "id" => (int)$id
                            ),
                            "id" => "Get".ucwords($type)
                        )
                    )
                )
            )
        );
        $data = $this->send_json_query( $curl_data );
        die(json_encode($data->pdResponse->demands->demand[0]->$type));
    }

    /*
     * Get list of the objects in the account.
     */
    function getObjects($type = null)
    {
        if($type === null){
            if(true === isset($_POST['type']) && false === empty(trim($_POST['type']))){
                $type = $_POST['type'];
            }
        }
        $curl_data = json_encode(
            array(
                "pdRequest" => array(
                    "partnerGUID" => $this->pd_apikey,
                    "userCode" => $this->userCode,
                    "demands" => array(
                        "demand" => array(
                            "id" => "Get".$type
                        )
                    )
                )
            )
        );
        /*
         * Send request to get list of the objects.
         */
        $data = $this->send_json_query($curl_data);
        switch($type){
            case 'Surveys':
               $plural = 'surveys'; $single = 'survey';
               break;
            case 'Polls':
               $plural = 'polls'; $single = 'poll';
               break;
            case 'Quizzes':
               $plural = 'quizzes'; $single = 'quiz';
               break;
        }

        if (is_object($data) && isset($data->pdResponse->demands->demand)) {

            return $data->pdResponse->demands->demand[0]->$plural->$single;
        }
        else {
            $this->errors[]='no_getpolls';
            return 'error';
        }
    }

    /*
     * Get a userCode for the account.
     */
    function getUsercode(){
        $curl_data = json_encode(
            array(
                'pdInitiate' => array(
                    'partnerGUID' => $this->pd_apikey,
                    "partnerUserID" => "0",
                    "email" => $this->pd_email,
                    "password" => $this->pd_password
                )
            )
        );
        $data = $this->send_json_query( $curl_data );
        if ( false == isset( $data->pdResponse->userCode ) ) {
            return 'error';
        }
        return $data->pdResponse->userCode;
    }

    /*
     * Send a JSON request to the Polldaddy API and return the result as an object.
     */
    function send_json_query( $curl_data ) {
        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_URL, "https://api.polldaddy.com/" );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt( $ch, CURLOPT_POST, 1 );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $curl_data );
        $data = curl_exec( $ch );
        curl_close( $ch );
        return json_decode( $data );
    }
}
