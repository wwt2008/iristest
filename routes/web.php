<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'SurveyController@index');
Route::get('/surveys', 'SurveyController@surveys');
Route::get('/polls', 'SurveyController@polls');
Route::get('/quizzes', 'SurveyController@quizzes');

Route::post('/surveys/getobject', 'SurveyController@getObject');

Route::get('/home', 'SurveyController@index');
Route::post('/surveys/getuserid', 'SurveyController@getUsercode');
Route::post('/surveys/getobjects', 'SurveyController@getObjects');

Auth::routes();


